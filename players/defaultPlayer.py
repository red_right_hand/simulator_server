import simulator.functions as fun


class Player:
    def __init__(self):
        self.id = "Id1"
        self.name = "Default Player"
        self.team = "Team 1"
        self.cols_count = 0
        self.rows_count = 0
        self.x = 0
        self.y = 0
        self.tank_direction = fun.S
        self.gun_direction = fun.S
        self.map = ""

    # Executed at the beginning of the game.
    def set_init_values(self, player_id, team_name, cols_count, rows_count):
        self.id = player_id
        self.team = team_name
        self.cols_count = cols_count
        self.rows_count = rows_count

    # Executed at the beginning to get user name.
    def get_name(self):
        return self.name

    # Executed after killing other player.
    def set_information_about_killing(self):
        pass

    # Executed after tank death.
    def set_information_about_death(self):
        pass

    # Executed when tank had fault. (TANK FAULT RATE is 10% by default)
    def set_information_about_tank_fault(self):
        pass

    # Executed when move was wrong, for example when player wants to move out of map.
    def set_information_about_move_fault(self):
        pass

    # Executed on the end of the game.
    def set_information_about_game_end(self):
        pass

    # Executed at the beginning and after every move.
    def set_map_and_position(self, map, x, y, tank_direction, gun_direction, round):
        self.map = map
        self.x = x
        self.y = y
        self.tank_direction = tank_direction
        self.gun_direction = gun_direction

    # Executed before function get_next_move at the beginning of every round.
    def get_radio_message(self):
        return None

    # Executed at the beginning of every round to get player move.
    def get_next_move(self, radio_messages):
        return (fun.MOVE_FORWARD, 0, 0)
