import sys

from os import getcwd
from flask import Flask, send_from_directory, request
from .simulator import game

sys.path.append(".")
sys.path.append("./players")

app = Flask(__name__)

players = [
    "lkazmierczak",
    "jdarul",
    "ajamka",
    "jsypien",
    "mzielinski",
    "mwos",
    "mmatusiak",
    "rwcislo",
    "spiechaczek",
]


@app.route("/update-player/<player_id>", methods=["PUT"])
def updatePlayer(player_id):
    if player_id not in players:
        return "Invalid player_id", 404
    else:
        code = request.data.decode("utf-8")

        with open(f"./players/{player_id}.py", "w") as f:
            f.write(code)

        return "Good job!", 200


@app.route("/result/<result_id>", methods=["GET"])
def result(result_id):
    filename = result_id + ".zip"
    directory = f"{getcwd()}/games"

    return send_from_directory(directory, filename)


@app.route("/simulate/<game_count>", methods=["POST"])
def simulate(game_count):
    resultPath = game.main("config", int(game_count))
    return resultPath
