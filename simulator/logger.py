import json
import datetime
from collections import OrderedDict

STATUS_ALIVE = "STATUS_ALIVE"
STATUS_DEAD = "STATUS_DEAD"


class Logger:
    rounds = []
    players = []

    def __init__(self):
        self.rounds = []
        # (player_id, player_name, player_team, kills, death_round, killer_id)
        # kills is a list of tuples (round, victim_id)
        self.players = []

    def add_round(self, round):
        self.rounds.append(Round(round))

    def add_players(self, players):
        self.rounds[0].players.extend(players)
        for (id, name, team, _, _, _, _, _, _, _, _, _, _) in players:
            self.players.append((id, name, team, [], 0, ""))

    def add_players_to_round(self, round, players):
        self.rounds[round].players.extend(players)

    def add_message(self, round, message):
        self.rounds[round].messages.append(message)

    def add_kill(self, round, killer_id, victim_id):
        (_, _, _, killer_kills, _, _) = get_player_by_id(self.players, killer_id)
        killer_kills.append((round, victim_id))

        update_victim(self.players, victim_id, round, killer_id)

    def players_to_json(self):
        players_json = []

        for (
            player_id,
            player_name,
            player_team,
            kills,
            death_round,
            killer_id,
        ) in self.players:
            kills_json = []
            for (round, victim_id) in kills:
                kills_json.append({"round": round, "victim_id": victim_id})
            players_json.append(
                {
                    "player_id": player_id,
                    "player_name": player_name,
                    "player_team": player_team,
                    "death_round": death_round,
                    "killer_id": killer_id,
                    "kills": kills_json,
                }
            )

        return players_json


def get_player_by_id(player_list, player_id):
    """Method for getting a player by id from a list of players."""
    return next((x for x in player_list if x[0] == player_id), None)


def update_victim(player_list, victim_id, death_round, killer_id):
    victim = get_player_by_id(player_list, victim_id)
    player_list.remove(victim)
    (_, victim_name, victim_team, victim_kills, _, _) = victim
    player_list.append(
        (victim_id, victim_name, victim_team, victim_kills, death_round, killer_id)
    )


class Round(object):
    def __init__(self, round, players=[]):
        self.round = round
        self.messages = []
        self.players = []

    def to_file(self):
        # Convert players to json friendly format
        players_json = []

        for (
            id,
            name,
            team,
            tank_direction,
            gun_direction,
            x,
            y,
            status,
            color,
            step_type,
            value1,
            value2,
            value3,
        ) in self.players:
            if status == STATUS_ALIVE:
                players_json.append(
                    {
                        "id": id,
                        "name": name,
                        "team": team,
                        "tank_direction": tank_direction,
                        "gun_direction": gun_direction,
                        "x": x,
                        "y": y,
                        "color": color,
                        "last_step_type": step_type,
                        "last_step_value1": value1,
                        "last_step_value2": value2,
                        "last_step_value3": value3,
                    }
                )

        return {"round": self.round, "players": players_json, "messages": self.messages}
