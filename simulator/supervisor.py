import os
import json
import random

from .functions import *
from .logger import Logger
from copy import copy, deepcopy
from datetime import datetime


class Supervisor:
    def __init__(self, map, cols_count, rows_count, max_rounds, tank_fault_rate):
        self.map = map
        self.cols_count = cols_count
        self.rows_count = rows_count
        self.players = []
        self.round = 0
        self.logger = Logger()
        self.max_rounds = max_rounds
        self.tank_fault_rate = tank_fault_rate
        self.player_contexts = {}
        self.player_kills = {}

    # Creates copy of the map.
    def get_map_copy(self):
        return deepcopy(self.map)

    # Creates copy of the radio messages.
    def get_radio_copy(self, radio_messages):
        return deepcopy(radio_messages)

    # Creates copy of players list.
    def get_players_copy(self):
        copied_players = []
        for pl in self.players:
            (step_type, value1, value2) = pl.last_step
            copied_players.append(
                (
                    pl.id,
                    pl.name,
                    pl.team,
                    pl.tank_direction,
                    pl.gun_direction,
                    pl.x,
                    pl.y,
                    pl.status,
                    pl.color,
                    step_type,
                    value1,
                    value2,
                    pl.last_step_value3,
                )
            )

        return copied_players

    # Adds player to game.
    def add_player(self, player, id, team_name, x, y, color):
        j = random.randint(0, 3)
        tank_direction = get_tank_direction(j)
        j = random.randint(0, 7)
        gun_direction = get_gun_direction(j)

        player_name = player.get_name()

        self.player_kills[player_name] = 0

        self.players.append(
            GeneralPlayer(
                player,
                id,
                player_name,
                team_name,
                x,
                y,
                STATUS_ALIVE,
                tank_direction,
                gun_direction,
                color,
            )
        )
        player.set_init_values(id, team_name, self.cols_count, self.rows_count)
        self.map[y][x] = (id, player_name, team_name, tank_direction, gun_direction)

    # Starts next round.
    def next_round(self):
        self.round = self.round + 1

    def start_game(self):
        self.logger.add_round(self.round)
        self.logger.add_players(self.get_players_copy())

        # At the beginning update players map and positions.
        for general_player in self.players:
            general_player.update_position(self.get_map_copy(), 0)

        for _ in range(0, self.max_rounds):
            self.next_round()
            self.logger.add_round(self.round)

            print(">> Round '{0}' starts".format(self.round))
            players_steps = []
            radio_messages = []

            for general_player in self.players:
                # Ask for radio message only if player is alive
                if general_player.status == STATUS_ALIVE:
                    message = general_player.player.get_radio_message()
                    # Check if the message exists
                    if message is not None:
                        radio_messages.append(message)

            # Add radio messages to logs
            for message in radio_messages:
                self.logger.add_message(self.round, message)

            for general_player in self.players:
                # Get player's move only if he is alive!
                if general_player.status == STATUS_ALIVE:

                    move = general_player.player.get_next_move(
                        self.get_radio_copy(radio_messages)
                    )

                    name = general_player.name

                    mgr = self.player_contexts[name]["manager"]

                    if mgr.error is None:
                        players_steps.append((general_player, move))
                    elif general_player.status != STATUS_DEAD:
                        general_player.status = STATUS_DEAD
                        x, y = general_player.x, general_player.y
                        self.map[y][x] = EMPTY_FIELD_CHAR
                        general_player.player.set_information_about_death()

            # Sort steps: first turning tanks and guns, then moving and at the end shooting.
            players_steps = sorted(players_steps, key=sorted_by_move)

            for general_player, step in players_steps:
                if has_fault(self.tank_fault_rate):
                    general_player.player.set_information_about_tank_fault()
                    general_player.last_step = ("NONE", 0, 0)
                    general_player.last_step_value3 = 0
                    self.logger.add_message(
                        self.round, "Player '{0}' had fault".format(general_player.name)
                    )
                else:
                    self.make_step(general_player, step)
                    self.update_position(general_player)
                    # general_player.update_position()

            for general_player in self.players:
                general_player.update_position(self.get_map_copy(), self.round)

            self.logger.add_players_to_round(self.round, self.get_players_copy())

            # Check how many players left alive. If there is only one then finish the game.
            alive = get_count_of_alive_players(self.players)
            # print("Round: {0}, alive players: {1}".format(self.round, alive))
            if alive < 2:
                self.logger.add_message(
                    self.round, "Only {} player left!".format(alive)
                )
                break

        self.logger.add_message(self.round, "THE END")
        # Inform players about end.
        for general_player in self.players:
            general_player.player.set_information_about_game_end()

        alive = [p.name for p in self.players if p.status == STATUS_ALIVE]
        died = [p.name for p in self.players if p.status == STATUS_DEAD]
        failed = {
            p: ctx["manager"].error
            for p, ctx in self.player_contexts.items()
            if ctx["manager"].error is not None
        }

        return {
            "rounds": self.round,
            "alive": alive,
            "died": died,
            "failed": failed,
            "player_kills": self.player_kills,
        }

    def make_step(self, general_player, step):
        (step_type, value1, value2) = step
        if step_type == TURN_GUN_TO_LEFT or step_type == TURN_GUN_TO_RIGHT:
            # Calculate new position
            new_gun_direction = get_gun_direction_after_gun_rotation(
                general_player.gun_direction, step_type
            )
            # Add to logs
            self.logger.add_message(
                self.round,
                "'{0}': {1} from '{2}' to '{3}'".format(
                    general_player.name,
                    step_type,
                    general_player.gun_direction,
                    new_gun_direction,
                ),
            )
            # And turn
            general_player.gun_direction = new_gun_direction
            general_player.last_step = step

        elif step_type == TURN_TANK_TO_LEFT or step_type == TURN_TANK_TO_RIGHT:
            # Calculate new positions
            new_tank_direction = get_tank_direction_after_tank_rotation(
                general_player.tank_direction, step_type
            )
            new_gun_direction = get_gun_direction_after_tank_rotation(
                general_player.gun_direction, step_type
            )
            # Add to logs
            self.logger.add_message(
                self.round,
                "'{0}': {1} from '{2}' to '{3}'".format(
                    general_player.name,
                    step_type,
                    general_player.tank_direction,
                    new_tank_direction,
                ),
            )
            # And turn
            general_player.tank_direction = new_tank_direction
            general_player.gun_direction = new_gun_direction
            general_player.last_step = step

        elif step_type == MOVE_FORWARD or step_type == MOVE_BACK:
            (aim_x, aim_y) = get_position_after_move(
                general_player.x,
                general_player.y,
                general_player.tank_direction,
                step_type,
            )

            if is_free(self.map, aim_x, aim_y, self.cols_count, self.rows_count):
                # Add to logs
                self.logger.add_message(
                    self.round,
                    "'{0}': {1} from ({2},{3}) to ({4},{5})".format(
                        general_player.name,
                        step_type,
                        str(general_player.x),
                        str(general_player.y),
                        str(aim_x),
                        str(aim_y),
                    ),
                )
                # Move player on the map.
                self.move(general_player.x, general_player.y, aim_x, aim_y)
                # And update his position
                general_player.x = aim_x
                general_player.y = aim_y
                general_player.last_step = (step_type, aim_x, aim_y)
            else:
                general_player.player.set_information_about_move_fault()
                self.logger.add_message(
                    self.round,
                    "WARNING: '{0}' cannot move from ({1},{2}) to ({3},{4}). The field is not empty!".format(
                        general_player.name,
                        str(general_player.x),
                        str(general_player.y),
                        str(aim_x),
                        str(aim_y),
                    ),
                )

        elif step_type == SHOOT:
            # Add to logs
            self.logger.add_message(
                self.round,
                "'{0}': {1} at ({2},{3})".format(
                    general_player.name, step_type, str(value1), str(value2)
                ),
            )

            if is_aim_in_right_direction(
                general_player.x,
                general_player.y,
                general_player.gun_direction,
                value1,
                value2,
            ):
                (aim_x, aim_y) = get_shoot_aim(
                    self.map,
                    self.cols_count,
                    self.rows_count,
                    general_player.x,
                    general_player.y,
                    general_player.gun_direction,
                    value1,
                    value2,
                    False,
                )
                general_player.last_step = (step_type, aim_x, aim_y)
                general_player.last_step_value3 = 0

                if isinstance(self.map[aim_y][aim_x], tuple):
                    (player_id, player_name, _, _, _) = self.map[aim_y][aim_x]
                    # Remove player from the map
                    player = get_player_by_id(self.players, player_id)
                    player.status = STATUS_DEAD
                    self.map[aim_y][aim_x] = EMPTY_FIELD_CHAR
                    player.player.set_information_about_death()

                    print(
                        ">>>> '{0}' killed by '{1}'".format(
                            player_id, general_player.name
                        )
                    )

                    self.player_kills[general_player.name] += 1

                    # Add to logs
                    self.logger.add_message(
                        self.round,
                        "'{0}': shoot player '{1}' at ({2},{3})".format(
                            general_player.name, player_name, aim_x, aim_y
                        ),
                    )
                    self.logger.add_kill(self.round, general_player.id, player_id)
                    general_player.last_step_value3 = 1
                    general_player.player.set_information_about_killing()
                else:
                    # Add to logs
                    self.logger.add_message(
                        self.round,
                        "WARNING: no one killed at ({0},{1}). The field is empty or there is a wall.".format(
                            str(value1), str(value2)
                        ),
                    )
            else:
                general_player.player.set_information_about_move_fault()
                # Add to logs
                self.logger.add_message(
                    self.round,
                    "Error: '{0}' cannot shoot at ({1},{2}). Incorrect gun direction and aim.".format(
                        general_player.name, str(value1), str(value2)
                    ),
                )

    def move(self, from_x, from_y, to_x, to_y):
        self.map[to_y][to_x] = self.map[from_y][from_x]
        self.map[from_y][from_x] = EMPTY_FIELD_CHAR

    def update_position(self, general_player):
        self.map[general_player.y][general_player.x] = (
            general_player.id,
            general_player.name,
            general_player.team,
            general_player.tank_direction,
            general_player.gun_direction,
        )

    def save_game_to_file(self, directory="games", game_number=0):
        filename = "tankgame_{1}_{0:%Y_%m_%d__%H_%M_%S}.json".format(
            datetime.now(), game_number
        )
        here = os.path.dirname(os.path.realpath(__file__))
        filepath = os.path.join(here, directory, filename)

        # create your subdirectory
        if not os.path.exists(os.path.join(here, directory)):
            os.makedirs(os.path.join(here, directory))

        file = open(filepath, "w")
        rounds = []
        for round in self.logger.rounds:
            rounds.append(round.to_file())

        copied_map = deepcopy(self.map)
        for i in range(self.rows_count):
            for j in range(self.cols_count):
                if not isinstance(copied_map[i][j], str):
                    copied_map[i][j] = EMPTY_FIELD_CHAR

        data = {
            "config": {
                "cols_count": self.cols_count,
                "rows_count": self.rows_count,
                "max_rounds": self.max_rounds,
                "rounds_count": len(rounds),
                "map": copied_map,
            },
            "rounds": rounds,
            "result": self.logger.players_to_json(),
        }

        file.write(json.dumps(data, indent=0))
        file.close()

        return {}


class GeneralPlayer:
    def __init__(
        self,
        player,
        id,
        player_name,
        team_name,
        x,
        y,
        status,
        tank_direction,
        gun_direction,
        color,
    ):
        self.id = id
        self.player = player
        self.x = x
        self.y = y
        self.tank_direction = tank_direction
        self.gun_direction = gun_direction
        self.name = player_name
        self.team = team_name
        self.status = status
        self.color = color
        self.last_step = ("NONE", 0, 0)
        self.last_step_value3 = 0

    def update_position(self, map, round):
        self.player.set_map_and_position(
            map, self.x, self.y, self.tank_direction, self.gun_direction, round
        )


def has_fault(fault_rate):
    j = random.random()
    return j <= fault_rate


def get_player_by_id(player_list, value):
    """Method for getting a player by id from a list of players."""
    return next((x for x in player_list if x.id == value), None)


# Method for sorting list of player steps.
def sorted_by_move(player_step):
    (_, (step_type, _, _)) = player_step
    if step_type.startswith("TURN"):
        return 0
    if step_type.startswith("MOVE"):
        return 0.5
    return 1


def get_count_of_alive_players(general_player_list):
    return sum(pl.status == STATUS_ALIVE for pl in general_player_list)
