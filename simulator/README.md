# Tank Simulator
## How to use?
### Upload your tank

1. Your code has to be implemented in a single file e.g. "cowardPlayer.py"
2. If you use some of the functions that were already defined in the code (e.g.
   in the "functions" module) your imports need to be relative to the "simulator"
   module".
   
   For example if I were to use a function "can_shoot_someone" from the
   "functions" I could do that by importing:
   
   ``` python
   
  import simulator.functions as fun
  
  fun.can_shoot_someone(...)
```

3. Make the following HTTP PUT request: 

``` sh
curl -X PUT -H "Content-Type: text/plain" http://34.89.239.205:80/update-player/player_id --date-binary "@path/to/your/player/class"
```
   
Example:
``` sh
curl -X PUT -H "Content-Type: text/plain" http://34.89.239.205:80/update-player/lkazmierczak --data-binary "@cowardPlayer.py"
```

List of the nicks:
- lkazmierczak
- jdarul
- ajamka
- jsypien
- mzielinski
- mwos
- mmatusiak
- rwcislo
- spiechaczek

### Run a simulation

Make the following HTTP POST request:

``` sh
curl -X POST http://34.89.239.205:80/simulate/number_of_games_to_simulate
```

Example:

``` sh
curl -X POST http://34.89.239.205:80/simulate/1
```

You should get something that looks like this:

``` json
{
    "games": [
        {
            "alive": [
                "lkazmierczak-1",
                "ajamka-1",
                "mzielinski-1",
                "mwos-1",
                "ajamka-2",
                "jsypien-2",
                "mwos-2",
                "mmatusiak-2",
                "rwcislo-2",
                "spiechaczek-2",
                "jsypien-3",
                "mzielinski-3",
                "mwos-3",
                "mmatusiak-3",
                "lkazmierczak-4",
                "jdarul-4",
                "mmatusiak-4",
                "rwcislo-4"
            ],
            "died": [
                "jdarul-1",
                "jsypien-1",
                "mmatusiak-1",
                "rwcislo-1",
                "spiechaczek-1",
                "bot-1",
                "lkazmierczak-2",
                "jdarul-2",
                "mzielinski-2",
                "bot-2",
                "lkazmierczak-3",
                "jdarul-3",
                "ajamka-3",
                "rwcislo-3",
                "spiechaczek-3",
                "bot-3",
                "ajamka-4",
                "jsypien-4",
                "mzielinski-4",
                "mwos-4",
                "spiechaczek-4",
                "bot-4"
            ],
            "failed": {
                "bot-1": "Error in get_next_move: I'm done.",
                "bot-2": "Error in get_next_move: I'm done.",
                "bot-3": "Error in get_next_move: I'm done.",
                "bot-4": "Error in get_next_move: I'm done."
            },
            "game_number": 0,
            "player_kills": {
                "ajamka-1": 1,
                "ajamka-2": 1,
                "ajamka-3": 0,
                "ajamka-4": 0,
                "bot-1": 0,
                "bot-2": 0,
                "bot-3": 0,
                "bot-4": 0,
                "jdarul-1": 0,
                "jdarul-2": 0,
                "jdarul-3": 0,
                "jdarul-4": 1,
                "jsypien-1": 0,
                "jsypien-2": 0,
                "jsypien-3": 0,
                "jsypien-4": 0,
                "lkazmierczak-1": 2,
                "lkazmierczak-2": 0,
                "lkazmierczak-3": 1,
                "lkazmierczak-4": 2,
                "mmatusiak-1": 0,
                "mmatusiak-2": 0,
                "mmatusiak-3": 1,
                "mmatusiak-4": 2,
                "mwos-1": 0,
                "mwos-2": 1,
                "mwos-3": 1,
                "mwos-4": 0,
                "mzielinski-1": 2,
                "mzielinski-2": 0,
                "mzielinski-3": 2,
                "mzielinski-4": 0,
                "rwcislo-1": 0,
                "rwcislo-2": 0,
                "rwcislo-3": 0,
                "rwcislo-4": 0,
                "spiechaczek-1": 0,
                "spiechaczek-2": 1,
                "spiechaczek-3": 0,
                "spiechaczek-4": 0
            },
            "rounds": 10
        }
    ],
    "result_id": "game_config_2019_11_30__19_22"
}
```

### Get results
1. Run a simulation as shown in the previous section
2. Take the "result_id" that was returned as a part of JSON
3. Run the "wget" command (or an alternative thatyou like)

``` sh
wget http://34.89.239.205:80/result/result_id
```

Example:
``` sh
wget http://34.89.239.205:80/result/game_config_2019_11_30__19_081
```


As a result you'll get a .zip file (even though it has no extension).

You can then use "unzip" to extract the results.

Example:

```sh
unzip game_config_2019_11_30__19_081
```

