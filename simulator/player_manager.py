#!/usr/bin/env python3

import inspect


class PlayerManager:
    def __init__(self):
        self.error = None


def make_safe(f, method_name, player_mgr):
    def _f(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            print(f"Exception was thrown during {method_name}: {e}")
            player_mgr.error = f"Error in {method_name}: {e}"

    return _f


def wrap_player(p, player_mgr):
    for name, mem in inspect.getmembers(p):
        if inspect.ismethod(mem):
            current = getattr(p, name)
            setattr(p, name, make_safe(current, name, player_mgr))

    return p
