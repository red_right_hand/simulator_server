from copy import copy, deepcopy

STATUS_ALIVE = "STATUS_ALIVE"
STATUS_DEAD = "STATUS_DEAD"

TURN_GUN_TO_LEFT = "TURN_GUN_TO_LEFT"
TURN_GUN_TO_RIGHT = "TURN_GUN_TO_RIGHT"
TURN_TANK_TO_LEFT = "TURN_TANK_TO_LEFT"
TURN_TANK_TO_RIGHT = "TURN_TANK_TO_RIGHT"
MOVE_FORWARD = "MOVE_FORWARD"
MOVE_BACK = "MOVE_BACK"
SHOOT = "SHOOT"

N = "N"
NE = "NE"
E = "E"
SE = "SE"
S = "S"
SW = "SW"
W = "W"
NW = "NW"

BUILDING_CHAR = 'b'
WATER_CHAR = 'w'
EMPTY_FIELD_CHAR = '_'

FIELD_ON_THE_LEFT = "FIELD_ON_THE_LEFT"
FIELD_ON_THE_RIGHT = "FIELD_ON_THE_RIGHT"
FIELD_IN_THE_FRONT = "FIELD_IN_THE_FRONT"
FIELD_IN_THE_BACK = "FIELD_IN_THE_BACK"

def are_coordinates_correct(x: int, y: int, cols_count: int, rows_count: int):
    """Check if the coordinates are correct."""
    if x >= 0 and x < cols_count and y >= 0 and y < rows_count:
        return True
    return False

def is_free(map: list, x: int, y: int, cols_count: int, rows_count: int):
    """Check if the field is free and player can move to the place."""
    if are_coordinates_correct(x, y, cols_count, rows_count):
        if map[y][x] == '_':
            return True
    return False

def get_tank_direction_after_tank_rotation(current_tank_direction: str, step_type: str):
    """Return tank direction after tank rotation. Step_type is TURN_TANK_TO_LEFT or TURN_TANK_TO_RIGHT."""
    choices = {"N": ("W", "E"), "E": ("N", "S"), "S": ("E", "W"), "W": ("S", "N")}
    (left, right) = choices.get(current_tank_direction, ("N", "N"))

    if step_type == TURN_TANK_TO_LEFT:
        return left

    return right

def get_tank_direction(number: int):
    """Return tank direction. 0 -> N, 1 -> E, 2 -> S, 3 -> W."""
    choices = {0: ("N"), 1: ("E"), 2: ("S"), 3: ("W")}
    return choices.get(number, (""))

def get_gun_direction_after_tank_rotation(current_gun_direction: str, step_type: str):
    """Return gun direction after tank rotation. Step_type is TURN_TANK_TO_LEFT or TURN_TANK_TO_RIGHT."""
    choices = {"N": ("W", "E"), "NE": ("NW", "SE"), "E": ("N", "S"), "SE": ("NE", "SW"), "S": ("E", "W"), "SW": ("SE", "NW"), "W": ("S", "N"), "NW": ("SW", "NE")}
    (left, right) = choices.get(current_gun_direction, ("N", "N"))

    if step_type == TURN_TANK_TO_LEFT:
        return left

    return right

def get_gun_direction_after_gun_rotation(current_gun_direction: str, step_type: str):
    """Return gun direction after gun rotation. Step_type is TURN_GUN_TO_LEFT or TURN_GUN_TO_RIGHT."""
    choices = {"N": ("NW", "NE"), "NE": ("N", "E"), "E": ("NE", "SE"), "SE": ("E", "S"), "S": ("SE", "SW"), "SW": ("S", "W"), "W": ("SW", "NW"), "NW": ("W", "N")}
    (left, right) = choices.get(current_gun_direction, ("N", "N"))

    if step_type == TURN_GUN_TO_LEFT:
        return left

    return right

def get_gun_directions_after_n_gun_rotations(current_gun_direction: str, step_type: str, moves_count: int):
    """Return gun directions after N gun rotations. Step_type is TURN_GUN_TO_LEFT or TURN_GUN_TO_RIGHT."""
    gun_directions = []
    new_gun_direction = current_gun_direction
    for _ in range(0, moves_count):
        new_gun_direction = get_gun_direction_after_gun_rotation(new_gun_direction, step_type)
        gun_directions.append(new_gun_direction)

    return gun_directions

def get_gun_direction(number: int):
    """Return proper gun direction. For example 0 is N, 1 is NE etc."""
    choices = {0: ("N"), 1: ("NE"), 2: ("E"), 3: ("SE"), 4: ("S"), 5: ("SW"), 6: ("W"), 7: ("NW")}
    return choices.get(number, (""))

def get_opposite_gun_direction(gun_direction: str):
    """Return opposite gun direction. For example for N it is S."""
    choices = {"S": ("N"), "SW": ("NE"), "W": ("E"), "NW": ("SE"), "N": ("S"), "NE": ("SW"), "E": ("W"), "SE": ("NW")}
    return choices.get(gun_direction, (""))

def get_position_after_move(x: int, y: int, tank_direction: str, step_type: str):
    """Return position where a player will move according to step type and tank direction. Step_type is MOVE_FORWARD or MOVE_BACK."""
    aim_x = x
    aim_y = y
    if step_type == MOVE_FORWARD:
        if tank_direction == N:
            aim_y -= 1
        elif tank_direction == E:
            aim_x += 1
        elif tank_direction == S:
            aim_y += 1
        elif tank_direction == W:
            aim_x -= 1
    elif step_type == MOVE_BACK:
        if tank_direction == N:
            aim_y += 1
        elif tank_direction == E:
            aim_x -= 1
        elif tank_direction == S:
            aim_y -= 1
        elif tank_direction == W:
            aim_x += 1

    return (aim_x, aim_y)

def is_aim_in_right_direction(from_x: int, from_y: int, gun_direction: str, to_x: int, to_y: int):
    """Check if the aim is in the correct direction."""
    if gun_direction == N:
        return from_x == to_x and from_y > to_y
    elif gun_direction == S:
        return from_x == to_x and from_y < to_y
    elif gun_direction == E:
        return from_y == to_y and from_x < to_x
    elif gun_direction == W:
        return from_y == to_y and from_x > to_x
    elif gun_direction == NE:
        # y = -x + b
        return (from_y + from_x) == (to_y + to_x) and from_x < to_x
    elif gun_direction == SW:
        # y = -x + b
        return (from_y + from_x) == (to_y + to_x) and from_x > to_x
    elif gun_direction == NW:
        # y = x + b
        return (from_y - from_x) == (to_y - to_x) and from_x > to_x
    else:
        # y = x + b
        return (from_y - from_x) == (to_y - to_x) and from_x < to_x

def get_shoot_aim(map: list, cols_count: int, rows_count: int, from_x: int, from_y: int, gun_direction: str, to_x: int, to_y: int, ignore_players: bool):
    """Get first position of wall or other player (if ignore_players is false) between current position and aim. If there is nothing between then return given aim."""
    (x_offset, y_offset) = get_offset_in_map(gun_direction)

    next_x = from_x + x_offset
    next_y = from_y + y_offset

    while (next_x != to_x or next_y != to_y) and next_x < cols_count and next_y < rows_count:
        if map[next_y][next_x] == BUILDING_CHAR:
            return (next_x, next_y)
        if (not ignore_players) and (isinstance(map[next_y][next_x], tuple) or isinstance(map[next_y][next_x], list)):
            return (next_x, next_y)
        next_x += x_offset
        next_y += y_offset

    return (to_x, to_y)

def is_some_player_on_line_of_fire(map: list, cols_count: int, rows_count: int, from_x: int, from_y: int, gun_direction: str, to_x: int, to_y: int):
    """Check if some player is on the line of fire"""
    (x_offset, y_offset) = get_offset_in_map(gun_direction)

    next_x = from_x + x_offset
    next_y = from_y + y_offset

    passed_aim = False

    while (not passed_aim) and next_x < cols_count and next_y < rows_count and next_x >= 0 and next_y >= 0:
        if isinstance(map[next_y][next_x], tuple):
            return True
        if isinstance(map[next_y][next_x], list):
            return True
        if map[next_y][next_x] == BUILDING_CHAR:
            return False
        if next_x == to_x and next_y == to_y:
            passed_aim = True
        next_x += x_offset
        next_y += y_offset

    return False

def is_the_player_on_line_of_fire(map: list, cols_count: int, rows_count: int, from_x: int, from_y: int, gun_direction: str, aim_player_id: str):
    """Check if the specific player is on the line of fire"""
    (x_offset, y_offset) = get_offset_in_map(gun_direction)

    next_x = from_x + x_offset
    next_y = from_y + y_offset

    while next_x < cols_count and next_y < rows_count and next_x >= 0 and next_y >= 0:
        if isinstance(map[next_y][next_x], tuple):
            (player_id, _, _, _, _) = map[next_y][next_x]
            if player_id == aim_player_id:
                return True
            else:
                return False
        if isinstance(map[next_y][next_x], list):
            for elem in map[next_y][next_x]:
                (player_id, _, _, _, _) = elem
                if player_id == aim_player_id:
                    return True
            return False
        if map[next_y][next_x] == BUILDING_CHAR:
            return False
        next_x += x_offset
        next_y += y_offset

    return False

def can_shoot_someone(map: list, cols_count: int, rows_count: int, from_x: int, from_y: int, gun_direction: str):
    """Check if from the field player can kill someone."""
    (to_x, to_y) = get_edge_of_map(cols_count, rows_count, from_x, from_y, gun_direction)
    return is_some_player_on_line_of_fire(map, cols_count, rows_count, from_x, from_y, gun_direction, to_x, to_y)

def get_offset_in_map(direction: str):
    """Return (x_offset, y_offset) in the direction. Works for gun and tank directions."""
    choices = {"N": (0, -1), "NE": (1, -1), "E": (1, 0), "SE": (1, 1), "S": (0, 1), "SW": (-1, 1), "W": (-1, 0), "NW": (-1, -1)}
    return choices.get(direction, (0, -1))

def get_move(number: int):
    """Return name of move by number, helpful in random moves."""
    choices = {0: TURN_GUN_TO_LEFT, 1: TURN_GUN_TO_RIGHT, 2: TURN_TANK_TO_LEFT, 3: TURN_TANK_TO_RIGHT, 4: MOVE_FORWARD, 5: MOVE_BACK, 6: SHOOT}
    return choices.get(number, 0)

def get_edge_of_map(cols_count: int, rows_count: int, current_x: int, current_y: int, direction: str):
    """Return the farthest possible field in the map. Direction can be tank or gun direction."""
    (x_offset, y_offset) = get_offset_in_map(direction)

    next_x = current_x + x_offset
    next_y = current_y + y_offset

    # Ok, we passed the edge already.
    if next_x > cols_count-1 or next_y > rows_count-1 or next_x < 0 or next_y < 0:
        return (current_x, current_y)

    while next_x < cols_count and next_y < rows_count and next_x >= 0 and next_y >= 0:
        next_x += x_offset
        next_y += y_offset

    next_x -= x_offset
    next_y -= y_offset
    return (next_x, next_y)

def get_players_from_map(map: list, cols_count: int, rows_count: int):
    """Return list of players from the map. Array of tuples (x, y, id, name, team, tank direction, gun direction)"""
    players = []

    for i in range(rows_count):
        for j in range(cols_count):
            if isinstance(map[i][j], tuple):
                (id, player_name, team_name, tank_direction, gun_direction) = map[i][j]
                players.append((j, i, id, player_name, team_name, tank_direction, gun_direction))
            if isinstance(map[i][j], list):
                for elem in map[i][j]:
                    (id, player_name, team_name, tank_direction, gun_direction) = elem
                    players.append((j, i, id, player_name, team_name, tank_direction, gun_direction))

    return players

def get_position_of_player(map: list, cols_count: int, rows_count: int, player_id: str):
    """Return position (x, y) of the player. If not found, return (-1, -1). 'Multiple map' not supported!"""

    for i in range(rows_count):
        for j in range(cols_count):
            if isinstance(map[i][j], tuple):
                (id, _, _, _, _) = map[i][j]
                if id == player_id:
                    return (j, i)

    return (-1, -1)

def get_players_targeting_the_field(players: list, map: list, cols_count: int, rows_count: int, x: int, y: int):
    """Return list of players that are targeting the field. Array of tuple (x, y, id, name, team, tank direction, gun direction)"""
    targeting_players = []

    for player in players:
        (j, i, id, player_name, team_name, tank_direction, gun_direction) = player

        if is_aim_in_right_direction(j, i, gun_direction, x, y):
            (aim_x, aim_y) = get_shoot_aim(map, cols_count, rows_count, j, i, gun_direction, x, y, True)

            if aim_x == x and aim_y == y:
                targeting_players.append((j, i, id, player_name, team_name, tank_direction, gun_direction))

    return targeting_players

def get_count_of_players_targeting_the_field(map: list, cols_count: int, rows_count: int, x: int, y: int):
    """Return count of players that can shoot at specific field"""
    players = get_players_from_map(map, cols_count, rows_count)
    targeting_players = get_players_targeting_the_field(players, map, cols_count, rows_count, x, y)
    return len(targeting_players)



def get_coordinates_by_field(x, y, current_direction, field):
    """Return coordinates depending on field and tank direction"""
    choices = {"N": ((0, -1), (1, 0), (0, 1), (-1, 0)), "E": ((1, 0), (0, 1), (-1, 0), (0, -1)), "S": ((0, 1), (-1, 0), (0, -1), (1, 0)), "W": ((-1, 0), (0, -1), (1, 0), (0, 1))}
    (front, right, back, left) = choices.get(current_direction, ((0, -1), (1, 0), (0, 1), (-1, 0)))

    (x_offset, y_offset) = (0, 0)
    if field == FIELD_IN_THE_FRONT:
        (x_offset, y_offset) = front
    elif field == FIELD_ON_THE_RIGHT:
        (x_offset, y_offset) = right
    elif field == FIELD_IN_THE_BACK:
        (x_offset, y_offset) = back
    elif field == FIELD_ON_THE_LEFT:
        (x_offset, y_offset) = left

    return (x + x_offset, y + y_offset)

def get_field_by_coordinates(x, y, current_direction, aim_x, aim_y):
    """Return field depending of coordinates and tank direction"""
    choices = {(0, -1): (FIELD_IN_THE_FRONT, FIELD_ON_THE_LEFT, FIELD_IN_THE_BACK, FIELD_ON_THE_RIGHT), (1, 0): (FIELD_ON_THE_RIGHT, FIELD_IN_THE_FRONT, FIELD_ON_THE_LEFT, FIELD_IN_THE_BACK), (0, 1): (FIELD_IN_THE_BACK, FIELD_ON_THE_RIGHT, FIELD_IN_THE_FRONT, FIELD_ON_THE_LEFT), (-1, 0): (FIELD_ON_THE_LEFT, FIELD_IN_THE_BACK, FIELD_ON_THE_RIGHT, FIELD_IN_THE_FRONT)}
    (north, east, south, west) = choices.get((aim_x - x, aim_y - y), (FIELD_IN_THE_FRONT, FIELD_ON_THE_LEFT, FIELD_IN_THE_BACK, FIELD_ON_THE_RIGHT))

    if current_direction == N:
        return north
    elif current_direction == E:
        return east
    elif current_direction == S:
        return south
    else:
        return west
