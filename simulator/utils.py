import os
from zipfile import ZipFile, ZIP_DEFLATED


def zipdir(source_path, target_path):
    with ZipFile(target_path, "w", ZIP_DEFLATED) as ziph:
        for root, dirs, files in os.walk(source_path):
            for file in files:
                ziph.write(os.path.join(root, file))
