from tkinter import Label, Menu, Frame, Text, Button, Scrollbar, Canvas
from tkinter import LEFT, TOP, BOTTOM, RIGHT, X, Y, END, NORMAL
from tkinter import N, S, E, W, NW
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror
from PIL import ImageTk, Image
import json

class MainFrame(Frame):
    def __init__(self):
        Frame.__init__(self)
        self.master.title("TankGame visualizer")

        # File menu.
        self.menubar = Menu(self)
        filemenu = Menu(self.menubar, tearoff=0)
        filemenu.add_command(label="Open File...", command=self.load_file)
        filemenu.add_command(label="Exit", command=self.quit)
        self.menubar.add_cascade(label="File", menu=filemenu)
        self.master.config(menu=self.menubar)

        # Top Label with round and buttons to change rounds.
        self.top_label = Label(self.master)
        self.top_label.pack(side=TOP)

        self.round_label = Label(self.top_label, text="Round: ?", fg="black")
        self.round_label.pack(fill=X)

        self.button_prev = Button(self.top_label, text = "<- Prev", command = self.load_prev_round, bg="grey")
        self.button_prev.pack(padx=5, pady=5, side=LEFT)
        self.button_zero = Button(self.top_label, text = "Zero", command = self.load_zero_round, bg="grey")
        self.button_zero.pack(padx=5, pady=5, side=LEFT)
        self.button_next = Button(self.top_label, text = "Next ->", command = self.load_next_round, bg="grey")
        self.button_next.pack(padx=5, pady=5, side=LEFT)
        self.button_zero = Button(self.top_label, text = "Last", command = self.load_last_round, bg="grey")
        self.button_zero.pack(padx=5, pady=5, side=LEFT)

        # Map label.
        self.map_label = Label(self.master)
        self.map_label.pack(side=TOP)
        self.canvas = Canvas(self.map_label, width=1, height=1)

        # Bottom label with messages from logger
        self.bottom_label = Label(self.master)
        self.bottom_label.pack(side=BOTTOM, fill=X)

        # self.title = Label(self.bottom_label, text="Messages from logger", fg="black")
        # self.title.pack()

        # self.scroll = Scrollbar(self.bottom_label)
        # self.messages = Text(self.bottom_label, height=6)
        # self.scroll.pack(side=RIGHT, fill=Y, pady=10)
        # self.messages.pack(side=BOTTOM, fill=X, pady=10, padx=10)
        # self.scroll.config(command=self.messages.yview)
        # self.messages.config(yscrollcommand=self.scroll.set)
        # quote = "Welcome to tank game visualizer!"
        # self.messages.insert(END, quote)

        self.rounds = []
        self.round = 0

        self.cols_count = 0
        self.rows_count = 0
        self.rounds_count = 0

        self.map = []

        self.images = []

    def load_file(self):
        fname = askopenfilename(filetypes=(("Json files", "*.json"), ("All files", "*.*") ))
        if fname:
            try:
                with open(fname) as f:
                    data = json.load(f)

                square_width=32
                square_height=32

                self.cols_count = data["config"]["cols_count"]
                self.rows_count = data["config"]["rows_count"]
                self.rounds_count = data["config"]["rounds_count"]
                self.map = data["config"]["map"]

                self.rounds = data["rounds"]

                self.canvas = Canvas(self.map_label, width=square_width*(self.cols_count+2), height=square_height*(self.rows_count+2))

                for y in range(1, self.rows_count+1):
                    for x in range(1, self.cols_count+1):

                        # Add color to map
                        color = ''
                        if self.map[y-1][x-1] == 'w': # Water
                            color = 'blue'
                        elif self.map[y-1][x-1] == 'b': # Building
                            color = 'grey'
                        
                        tag = "{0}_{1}".format(x-1, y-1)
                        self.canvas.create_rectangle(
                                    x*square_width, y*square_height,
                                    x*square_width+square_width, y*square_height+square_height,
                                    outline="black", fill=color, tag=tag)

                for x in range(1, self.cols_count+1):
                    self.canvas.create_text(x*square_width + (square_width/2), square_height - (square_height/2), text=str(x-1))
                    self.canvas.create_text(x*square_width + (square_width/2), (self.rows_count+1)*square_height + (square_height/2), text=str(x-1))
                for y in range(1, self.rows_count+1):
                    self.canvas.create_text(square_width - (square_width/2), y*square_height + (square_height/2), text=str(y-1))
                    self.canvas.create_text((self.cols_count+1)*square_width + (square_width/2), y*square_height + (square_height/2), text=str(y-1))
                
                # Add map to the layout.
                self.canvas.pack(side=BOTTOM)

                # At the beginning, load round 0.
                #self.load_zero_round()
            except:
                showerror("Open Source File", "Failed to read file\n'%s'" % fname)
            return

    def load_round(self, round):
        self.images = []
        # Delete all existing shooting lines.
        self.canvas.delete("shoot_line")
        players = self.rounds[round]["players"]
        for pl in players:
            x = pl["x"]
            y = pl["y"]
            tank_direction = get_tank_direction_int(pl["tank_direction"])
            gun_direction = get_gun_direction_int(pl["gun_direction"])
            item = self.canvas.find_withtag("{0}_{1}".format(x, y))
            coords = self.canvas.coords(item)
            if coords:
                # Special algorithm to calculate number of tank to show based on tank and gun directions.
                self.tankNumber = (gun_direction + (4 - tank_direction) * 2) % 8
                self.image = Image.open("./img/tank_{0}.gif".format(self.tankNumber))
                self.change_image_color(pl["color"], self.tankNumber)
                # -1 because I wanted to rotate to the right.
                angle = 90 * tank_direction * (-1)
                self.image = ImageTk.PhotoImage(self.image.rotate(angle, expand=True))
                self.images.append(self.image)
                self.canvas.create_image(coords[0], coords[1], image=self.images[-1], state=NORMAL, anchor=NW)

                # Visualize shooting.
                if pl["last_step_type"] == "SHOOT":
                    shoot_aim = self.canvas.find_withtag("{0}_{1}".format(pl["last_step_value1"], pl["last_step_value2"]))
                    aim_coords = self.canvas.coords(shoot_aim)
                    self.canvas.create_line(coords[0]+16, coords[1]+16, aim_coords[0]+16, aim_coords[1]+16, fill="red", width=2.0, tag="shoot_line")
                    if pl["last_step_value3"] == 1:
                        self.image = Image.open("./img/explosion.gif")
                        self.image = ImageTk.PhotoImage(self.image)
                        self.images.append(self.image)
                        self.canvas.create_image(aim_coords[0], aim_coords[1], image=self.images[-1], state=NORMAL, anchor=NW)

        self.round_label['text'] = "Round {0} of {1}".format(round, self.rounds_count - 1)
        self.round = round

        # Update Label with messages.
        # self.update_messages_label(round)
        self.write_messages(round)

    def write_messages(self, round):
        messages = self.rounds[round]["messages"]

        for msg in range(0, len(messages)):
            if msg == 0:
                print("\n----- Round {0} -----\n".format(round))
                print("> {0}".format(messages[msg]))
            else:
                print("> {0}".format(messages[msg]))

    # def update_messages_label(self, round):
    #     self.messages.delete('1.0', END)
    #     messages = self.rounds[round]["messages"]

    #     for msg in range(0, len(messages)):
    #         if msg == 0:
    #             self.messages.insert(END, "> {0}".format(messages[msg]))
    #         else:
    #             self.messages.insert(END, "\n> {0}".format(messages[msg]))

    def change_image_color(self, color, number):
        self.image = self.image.convert('RGBA')
        self.mask_image = Image.open("./img/tank_{0}_mask.gif".format(number))
        self.mask_image = self.mask_image.convert('RGBA')
        self.image.paste((color[0], color[1], color[2]), [0,0,self.image.size[0],self.image.size[1]], self.mask_image)

    def load_zero_round(self):
        self.load_round(0)

    def load_last_round(self):
        self.load_round(self.rounds_count - 1)
    
    def load_prev_round(self):
        if self.round > 1:
            self.load_round(self.round - 1)
        else:
            self.load_round(0)

    def load_next_round(self):
        if self.round < self.rounds_count - 1:
            self.load_round(self.round + 1)

def get_tank_direction_int(tank_direction):
    choices = {"N": 0, "E": 1, "S": 2, "W": 3}
    return choices.get(tank_direction, 0)

def get_gun_direction_int(gun_direction):
    choices = {"N": 0, "NE": 1, "E": 2, "SE": 3, "S": 4, "SW": 5, "W": 6, "NW": 7}
    return choices.get(gun_direction, 0)

if __name__ == "__main__":
    MainFrame().mainloop()