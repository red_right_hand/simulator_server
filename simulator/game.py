import sys
import re
import gc
import random

from os import getcwd
from datetime import datetime
from .supervisor import Supervisor
from .utils import zipdir
from .player_manager import *

from importlib import import_module, reload

# Config
TANK_FAULT_RATE = 0.1
EMPTY_FIELD_CHAR = "_"


def get_color_for_team(team_number, player_number):
    a = 20 * player_number
    if team_number == 1:
        return (255 - a, 0, 0)
    if team_number == 2:
        return (0, 255 - a, 0)
    if team_number == 3:
        return (0, 0, 255 - a)
    if team_number == 4:
        return (255 - a, 0, 255 - a)
    if team_number == 5:
        return (255 - a, 255 - a, 0)


def main(config_filename, count_of_games):
    sys.setrecursionlimit(2000)

    result_id = "game_{0}_{1:%Y_%m_%d__%H_%M}".format(
        config_filename.replace(".txt", ""), datetime.now()
    )

    directory_name = f"{getcwd()}/games/{result_id}"

    results = []

    for game_number in range(0, count_of_games):
        print("Game '{0}' starts".format(game_number))
        file = open(config_filename, "r")
        lines = file.readlines()

        # First line should be count of rounds
        l = 0
        max_rounds = int(lines[l])

        # Second line is size of map
        l = l + 1
        numbers = re.findall(r"\b\w+\b", lines[l])
        cols_count = int(numbers[0])
        rows_count = int(numbers[1])

        # Initialize map
        game_map = [
            [EMPTY_FIELD_CHAR for _ in range(cols_count)] for _ in range(rows_count)
        ]

        # Read map from file
        team_places = {}
        x = 0
        y = 0

        l = l + 1
        for i in range(l, rows_count + l):
            line = lines[i]

            for char in line:
                if char != "\n":
                    if char.isdigit():
                        if char in team_places:
                            team_places[char].append((x, y))
                        else:
                            team_places[char] = [(x, y)]
                    else:
                        game_map[y][x] = char
                    x = x + 1
            x = 0
            y = y + 1

        supervisor = Supervisor(
            game_map, cols_count, rows_count, max_rounds, TANK_FAULT_RATE
        )

        # Number of teams
        l = l + rows_count
        count_of_teams = int(lines[l])

        # Teams
        l = l + 1
        teams = {}
        t = 1
        for i in range(l, count_of_teams + l):
            line = lines[i]
            teams[str(t)] = lines[i].rstrip()
            t = t + 1

        # Number of players
        l = l + count_of_teams
        count_of_players = int(lines[l])

        # Players
        l = l + 1
        players_names = []
        teams_players = [0] * 10
        player_contexts = {}

        for i in range(l, count_of_players + l):
            p = lines[i].split()
            player_name = f"{p[0]}-{p[1]}"

            mgr = PlayerManager()
            player_contexts[player_name] = {"manager": mgr}

            try:
                player_module = import_module(p[0])
                reload(player_module)

                P = player_module.Player()
                P = wrap_player(P, mgr)
                P.name = player_name

                random.shuffle(team_places[str(p[1])])
                (x, y) = team_places[str(p[1])].pop()

                t = 1
                while player_name in players_names:
                    t = t + 1
                    player_name = "{0}_{1}".format(p[0], str(t))
                players_names.append(player_name)

                if teams_players[int(p[1])] is None:
                    teams_players[int(p[1])] = 0

                color = get_color_for_team(int(p[1]), teams_players[int(p[1])])
                teams_players[int(p[1])] = teams_players[int(p[1])] + 1

                supervisor.add_player(P, player_name, teams[p[1]], x, y, color)

            except Exception as e:
                mgr.error = f"Error while loading player {player_name}: {e}"

        supervisor.player_contexts = player_contexts

        result = supervisor.start_game()
        result["game_number"] = game_number
        results.append(result)

        supervisor.save_game_to_file(directory_name, game_number)

        print("Game '{0}' ends".format(game_number))

        # At the end of game, remove allocated unused memory.
        gc.collect()

    zipdir(directory_name, directory_name + ".zip")

    return {"result_id": result_id, "games": results}


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Correct usage: game <config file.txt> <count of games>")
        exit()
    main(sys.argv[1], int(sys.argv[2]))
